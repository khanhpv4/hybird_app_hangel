import { StyleSheet} from 'react-native';

export default StyleSheet.create({
    wrapper: {
        flex: 1,
        paddingTop: 34,
        backgroundColor: '#FA8704',
    },
    container: {
        flex: 1,
    },
});
