import { StyleSheet } from 'react-native';

export const CONTENT_PADDING_HZ = 50;

export default StyleSheet.create({
    container: {
        flex: 1,
        padding: CONTENT_PADDING_HZ,
        alignItems: 'center',
        alignContent:'center',

    },
    title: {
        fontSize: 16,
        textAlign: 'center',
        fontWeight:'bold',
        marginBottom: 30
    },
    examplesListContainer: {
        height: 85,
        flexGrow: 0
    },
    exampleBtn: {
        height: 50,
        padding: 10,
        marginHorizontal: 10,
        backgroundColor: 'teal',
        alignItems: 'center',
        justifyContent: 'center'
    },
    exampleBtnLabel: {
        fontSize: 13,
        color: 'white'
    }
});
