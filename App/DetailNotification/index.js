import React, {useEffect, useState} from 'react';
import {Dimensions, StyleSheet, Image, Text, View, ScrollView, useWindowDimensions} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import axios from 'axios';
import HTML from 'react-native-render-html';
import {CONTENT_PADDING_HZ} from './Style/style.js';

function DetailsNotification({route, navigation}) {
    const [spinner, setSpinner] = useState(false);
    const [title, setTitle] = useState('');
    const [image, setImage] = useState('');
    const [message, setMessage] = useState('');
    const [urlSetting, SetUrlSetting] = useState('https://api.hangel.vn/')
    const [urlImage, setUrlImage] = useState('https://cms.hangel.vn/')
    const {id} = route.params;
    const IMAGES_MAX_WIDTH = Dimensions.get('window').width - 50;
    const CUSTOM_STYLES = {};
    const CUSTOM_RENDERERS = {};
    const DEFAULT_PROPS = {
        htmlStyles: CUSTOM_STYLES,
        renderers: CUSTOM_RENDERERS,
        imagesMaxWidth: IMAGES_MAX_WIDTH,
        computeImagesMaxWidth(contentWidth) {
            return contentWidth - 40;
        },
        onLinkPress(evt, href) {
            Linking.openURL(href);
        },
        debug: true,
    };
    useEffect(() => {
        setSpinner(true);
        callApi();
    }, []);

    async function callApi() {
        let url = urlSetting + 'get_detail_notification?notification_id=' + id
        console.log(id)
        try {
            const res = await axios.get(url);
            if (res.data.status === 200) {
                setTitle(res.data.data.title);
                setImage(res.data.data.image ? res.data.data.image : null);
                setMessage(res.data.data.message);
                setSpinner(false)
                console.log(image);
            } else {
                setSpinner(false);
                alert("lổi dữ liệu");

            }

        } catch (error) {
            console.error(error);
        }
    }

    function fetchRow() {
        console.log("image")
        console.log(image)
        const {width: contentWidth} = useWindowDimensions();

        if (image != null) {
            return (
                <ScrollView>
                    <View style={styles.container}>
                        <Spinner visible={spinner}/>
                        <Image source={{uri: image}}/>
                        <Text style={styles.title_text}>{title}</Text>
                        <HTML
                            {...DEFAULT_PROPS}
                            contentWidth={contentWidth - CONTENT_PADDING_HZ * 2}
                            html={message} imagesMaxWidth={Dimensions.get('window').width}
                        />
                    </View>
                </ScrollView>
            )
        } else {
            return (
                <ScrollView>
                    <View style={styles.container}>
                        <Spinner visible={spinner}/>
                        <Image source={{uri: image}}/>
                        <Text style={styles.title_text}>{title}</Text>
                        <HTML
                            {...DEFAULT_PROPS}
                            contentWidth={contentWidth - CONTENT_PADDING_HZ * 2}
                            html={message} imagesMaxWidth={Dimensions.get('window').width}
                        />
                    </View>
                </ScrollView>
            )
        }

    }

    return (
        <>
            {fetchRow()}
        </>
    );
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF',
    },
    container: {
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
        marginLeft: 0,
        marginRight: 0,
        marginTop: 10,
        paddingLeft: '5%',
        paddingRight: '5%',
        overflow: 'scroll'

    },
    title_text: {
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'OpenSans',
        backgroundColor: 'transparent'

    },
    title_message: {
        fontSize: 14,
        fontFamily: 'OpenSans',
        backgroundColor: 'transparent'
    },
    logo: {
        width: '100%',
        height: 200,
        marginTop: 10,
        marginBottom: 10
    }
});


export default DetailsNotification;
