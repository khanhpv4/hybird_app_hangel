import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './Home';
import Welcome from './Welcome';
import DetailNotification from './DetailNotification';
import Notfound from './Notfound';
import { GoogleSignin } from '@react-native-community/google-signin';
const Stack = createStackNavigator();
function App() {
  GoogleSignin.configure({
    webClientId:
      '837286380268-mj222jttd5epmtvhs8t46ug8ham2flfi.apps.googleusercontent.com',
    offlineAccess: true,
  });
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Welcome"
        screenOptions={{
          headerShown: false,
          tabBarLabel: 'Settings!',
        }}>
        <Stack.Screen
          name="Welcome"
          component={Welcome}
          options={{
            title: 'Welcome',
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            headerShown: false,
            headerLeft: null,
            title: null,
          }}
        />
          <Stack.Screen
              name="DetailNotification"
              component={DetailNotification}
              options={{
                  footerShow:true,
                  headerShown: true,
                  headerTitleStyle: {
                      color: '#ffffff',
                      background:'#ffffff'
                  },
                  headerStyle: {
                      backgroundColor: '#13bdce'
                  },
                  title: 'Chi tiết thông báo',
              }}
          />
          <Stack.Screen
              name="Notfound"
              component={Notfound}
              options={{
                  headerLeft: null,
                  footerShow:true,
                  headerShown: true,
                  headerTitleStyle: {
                      color: '#ffffff',
                      background:'#ffffff'
                  },
                  headerStyle: {
                      backgroundColor: '#13bdce'
                  },
                  title: 'Thông báo',
              }}
          />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;
