import React, {useContext, useEffect, useState} from 'react';
import {Text, Image, View} from 'react-native';
import {Alert, CameraRoll, Platform, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';

function Welcome({navigation}) {
  const [spinner, setSpinner] = useState(false);
  useEffect(() => {
    setSpinner(true);
    setTimeout(function () {
      setSpinner(false);
      navigation.navigate('Home');
    }, 2000);
  }, []);
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Spinner
        visible={spinner}
        textContent={'Loading...'}
        textStyle={styles.spinnerTextStyle}
      />
      <Image
        source={require('./spash.jpg')}
        style={{width: '100%', height: '100%'}}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default Welcome;
