import React, {useContext, useEffect, useState,useCallback} from 'react';
import {Text, Image, View} from 'react-native';
import NetInfo from "@react-native-community/netinfo";


function NotFoundPage({navigation}){
    const [checkInternet,setCheckInternet] = useState(false)
    useEffect((props) => {
        NetInfo.addEventListener(state => {
            console.log(state.isConnected)
            if (state.isConnected === true) {
                navigation.push('Home')
            }
        });
    }, []);
    return(
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Image
                source={require('./internet_err.jpg')}
            />
        </View>
    )

}

export default NotFoundPage;
