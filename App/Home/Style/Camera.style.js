import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop: 34,
    backgroundColor: '#FA8704',
  },
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
  },
  notification: {
    width: 150,
    backgroundColor: '#FA8704',
    textAlign: 'center',
    borderColor:'#cccc',
    borderWidth:1,
    justifyContent:'space-between',
    alignContent:'center',
    flexDirection:'row',
  },
  popup_div: {
   zIndex:1000,
    height:50,
    position:'absolute',
    top:5,
    bottom:0

  }
});
