import React, {Component} from 'react';
import OneSignal from 'react-native-onesignal'; // Import package from node modules
import {
  Alert,
  BackHandler,
  Image,
  PermissionsAndroid,
  Text,
  View,
} from 'react-native';
import {AndroidBackHandler} from 'react-navigation-backhandler';
import {WebView} from 'react-native-webview';
import styles from '../Home/Style/Camera.style';
import axios from 'axios';
import NetInfo from '@react-native-community/netinfo';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';

const ONE_SIGNAL_APP_ID = '2d3662ea-6a75-4257-ba38-19ad9393a445';

const JSCODE = `document.addEventListener("message", function (event) {
  console.log('bbbb', event);
});`;
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.initOneSignal();
    this.state = {
      yourInitialUrl: 'https://hangel.vn/',
      // yourInitialUrl: 'http://192.168.1.17:3000/',
      key: 1,
      isWebViewUrlChanged: false,
      userID: '',
      deviceToken: null,
      isConnected: true,
      webviewState: true,
      showWebview: false,
      loading: false,
    };
    this.WEBVIEW_REF = React.createRef();

    //Remove this method to stop OneSignal Debugging
    OneSignal.setLogLevel(6, 0);

    // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
    OneSignal.init('2d3662ea-6a75-4257-ba38-19ad9393a445', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2); // Controls what should happen if a notification is received while the app is open. 2 means that the notification will go directly to the device's notification center.

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    this.requestLocationPermission();
    const {navigation} = this.props;
    NetInfo.addEventListener((state) => {
      console.log(state.isConnected);
      if (state.isConnected === false) {
        this.focusListener = navigation.addListener('didFocus', () => {
          this.setState({count: 0});
        });
        navigation.navigate('Notfound');
      }
    });
  }

  componentDidMount() {}

  initOneSignal = () => {
    // OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.DEBUG)
    OneSignal.init(ONE_SIGNAL_APP_ID, {
      kOSSettingsKeyAutoPrompt: true,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.enableVibrate(true);
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  };
  handleConnectivityChange = (isConnected) => {
    this.setState({isConnected});
  };
  requestLocationPermission = () => {
    let that = this;
    const granted2 = PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Ứng dụng cần ghi nhận vị trí của ',
        message: 'Vui lòng cấp quyền truy cập vị trí của bạn',
      },
    );
    if (granted2 === PermissionsAndroid.RESULTS.GRANTED) {
      that.proceed();
    }
  };

  onReceived(notification) {
    console.log(notification);
    console.log('Notification received: ', notification.payload);
  }

  onOpened = (openResult) => {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
    console.log('click noti');
    console.log(openResult.notification.payload.additionalData.id);
    console.log(openResult.notification.payload.additionalData.id);
    let idNoti = openResult.notification.payload.additionalData.id;
    const {navigation} = this.props;
    navigation.navigate('DetailNotification', {id: idNoti});
  };

  onIds = (device) => {
    console.log('Device info: ', device);
    this.setState({deviceToken: device.userId});
  };

  setWebViewUrlChanged = (webviewState) => {
    if (webviewState.url !== this.state.yourInitialUrl) {
      this.setState({isWebViewUrlChanged: true});
    }
  };

  render() {
    return (
      <>
        <AndroidBackHandler onBackPress={this.onHardwareBackButton}>
          {this.state.loading && <Text>Loading...</Text>}
          <WebView
            ref={this.WEBVIEW_REF}
            style={styles.container}
            key={this.state.key}
            sharedCookiesEnabled={true}
            useWebKit={true}
            allowUniversalAccessFromFileURLs
            source={{uri: this.state.yourInitialUrl}}
            allowFileAccess={true}
            mixedContentMode="compatibility"
            onNavigationStateChange={this.setWebViewUrlChanged}
            onError={(e) =>
              Alert.alert(
                'Thông báo',
                'Vui lòng kiểm tra đường truyền internet!',
              )
            }
            javaScriptEnabled={true}
            onMessage={this.onMessage}
            injectedJavaScript={JSCODE}
          />
        </AndroidBackHandler>
      </>
    );
  }

  onHardwareBackButton = () => {
    this.WEBVIEW_REF.current.goBack();
    return true;
  };

  sendMessageToWebView(message) {
    if (this.WEBVIEW_REF.current) {
      console.log('test', message);
      this.WEBVIEW_REF.current.postMessage(message);
    }
  }

  loginFacebook() {
    this.setState({loading: true});
    LoginManager.logInWithPermissions(['public_profile']).then(
      (result) => {
        if (result.isCancelled) {
          console.log('Login cancelled');
          this.setState({loading: false});
        } else {
          AccessToken.getCurrentAccessToken()
            .then(({accessToken}) => {
              const message = {
                type: 'facebook_login_token',
                value: accessToken,
              };
              this.sendMessageToWebView(JSON.stringify(message));
            })
            .catch((err) => {
              alert('Login Facebook failed.');
              console.log('fb_get_access_token', err);
            })
            .finally(() => {
              this.setState({loading: false});
            });
        }
      },
      (error) => {
        alert('Login Facebook failed.');
        console.log('Login fail with error: ' + error);
        this.setState({loading: false});
      },
    );
  }

  async loginGoogle() {
    this.setState({loading: true});
    try {
      await GoogleSignin.hasPlayServices();
      let {accessToken, idToken} = await GoogleSignin.signIn();
      if (!accessToken || !idToken) {
        const tokens = await GoogleSignin.getTokens();
        accessToken = tokens.accessToken;
        idToken = tokens.idToken;
      }
      const message = {
        type: 'google_login_token',
        accessToken,
        idToken,
      };
      this.sendMessageToWebView(JSON.stringify(message));
    } catch (error) {
      console.log('login_google_error', error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        // alert('Cancel');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        alert('Signin in progress');
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        alert('PLAY_SERVICES_NOT_AVAILABLE');
        // play services not available or outdated
      } else {
        // some other error happened
      }
    } finally {
      this.setState({loading: false});
    }
  }

  onMessage = (e) => {
    const data = e.nativeEvent.data;
    console.log('%conMessage', 'color:blue', e.nativeEvent.data);
    switch (data) {
      case 'login_facebook':
        this.loginFacebook();
        break;
      case 'login_google':
        this.loginGoogle();
        break;
    }
    this.setState({userID: e.nativeEvent.data});
    if (this.state.userID != null) {
      this.saveDeviceToken();
    }
  };

  saveDeviceToken = async () => {
    console.log('this.state.deviceToken' + this.state.deviceToken);
    console.log('this.state.user' + this.state.userID);
    if (this.state.userID != null) {
      try {
        const res = await axios.post(
          'https://api.hangel.vn/update_device_token',
          {
            user_id: this.state.userID,
            device_token: this.state.deviceToken,
          },
        );
        console.log(res);
      } catch (e) {
        console.error(e);
      }
    }
  };

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }
}
